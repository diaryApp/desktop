import React, { Component } from 'react';
import './App.css';
import {Grid,Row,Col,FormGroup,FormControl,ListGroup,ListGroupItem,Button,ControlLabel,Glyphicon} from "react-bootstrap";
var client = require('./client.js');
var moment = require('moment');
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(':memory:');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: "",
      notes: []
    };
  }
  successDB(){

  }
  errorDB(){

  }
  componentWillMount(){
    this.loadNote();
  }
  loadNote(){
    client.getRequest('note')
      .then((results) => {
        this.setState({
          notes: results
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  saveNote(){
    client.postRequest('note',{content:this.state.content});
    this.loadNote();
  }
  selectNote(){

  }
  render() {
    var list = [];
    for(let i=0;i<this.state.notes.length;i++){
      list.push(<ListGroupItem bsClass="listItem" header={this.state.notes[i].content.substring(0,50)} key={this.state.notes[i].id}>{this.state.notes[i].created.substring(0,10)}</ListGroupItem>);
    }
    return (
      <Grid>
          <Row className="show-grid">
            <Col md={4}>
              <ListGroup id="list">{list}</ListGroup>
            </Col>
            <Col md={8} id="writepad">
              <form>
                <FormGroup>
                  <Row style={{marginTop:10}}>
                    <Col md={1}>
                      <Button><Glyphicon className="glyphicon" glyph="pencil" onClick={this.saveNote.bind(this)} /></Button>
                    </Col>
                    <Col md={10}>
                      <ControlLabel id="date">{moment().format('Do MMMM, h:mm a')}</ControlLabel>
                    </Col>
                    <Col md={1}>
                      <Button><Glyphicon className="glyphicon" glyph="trash" /></Button>
                    </Col>
                  </Row>
                  <FormControl style={{marginTop:10}} componentClass="textarea" id="textarea" value={this.state.content} onChange={(event) => {this.setState({content: event.target.value})}}/>
                </FormGroup>
              </form>
            </Col>
          </Row>
      </Grid>
    );
  }
}

export default App;
